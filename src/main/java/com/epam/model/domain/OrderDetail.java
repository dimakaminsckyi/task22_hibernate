package com.epam.model.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "order_detail")
public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "start_point", nullable = false)
    private String startPoint;
    @Column(name = "end_point", nullable = false)
    private String endPoint;
    @Column(name = "city", nullable = false)
    private String city;
    @Column(name = "price", length = 250, nullable = false)
    private int price;
    @Column(name = "payment", nullable = false)
    private Payment payment;
    @Column(name = "date_of_order" , length = 8, nullable = false)
    private String dateOfOrder;
    @Column(name = "rating" ,length = 5)
    private int rating;
    @Column(name = "order_status", nullable = false)
    private boolean orderStatus;
    @ManyToMany
    @JoinTable(
            name = "order_detail_driver",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "driver_id")}
    )
    private Set<Driver> drivers = new HashSet<>();
    @ManyToMany
    @JoinTable(
            name = "order_detail_customer",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "customer_id")}
    )
    private Set<Customer> customers = new HashSet<>();
    @ManyToMany
    @JoinTable(
            name = "order_detail_business_customer",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "business_customer_id")}
    )
    private Set<BusinessCustomer> businessCustomers = new HashSet<>();



    public OrderDetail() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(String dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(boolean orderStatus) {
        this.orderStatus = orderStatus;
    }


    @Override
    public String toString() {
        return "OrderDetail{" +
                "id=" + id +
                ", startPoint='" + startPoint + '\'' +
                ", endPoint='" + endPoint + '\'' +
                ", city='" + city + '\'' +
                ", price=" + price +
                ", payment=" + payment +
                ", dateOfOrder='" + dateOfOrder + '\'' +
                ", rating=" + rating +
                ", orderStatus=" + orderStatus +
                '}';
    }
}

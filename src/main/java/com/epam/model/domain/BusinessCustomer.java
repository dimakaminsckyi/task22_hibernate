package com.epam.model.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "business_customer")
public class BusinessCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "business_customer_name")
    private String businessCustomerName;
    @Column(name = "phone_number", length = 10)
    private String phoneNumber;
    @OneToOne
    @JoinColumn(name = "password_id", nullable = false)
    private SecurePassword password;
    @OneToOne
    @JoinColumn(name = "card_id", nullable = false)
    private CreditCard card;
    @ManyToMany(mappedBy = "businessCustomers")
    private Set<OrderDetail> orderDetails = new HashSet<>();

    public BusinessCustomer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBusinessCustomerName() {
        return businessCustomerName;
    }

    public void setBusinessCustomerName(String businessCustomerName) {
        this.businessCustomerName = businessCustomerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public SecurePassword getPassword() {
        return password;
    }

    public void setPassword(SecurePassword password) {
        this.password = password;
    }

    public CreditCard getCard() {
        return card;
    }

    public void setCard(CreditCard card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "BusinessCustomer{" +
                "id=" + id +
                ", businessCustomerName='" + businessCustomerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", password=" + password +
                ", card=" + card +
                '}';
    }
}

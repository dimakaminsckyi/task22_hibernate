package com.epam.model.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "driver")
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "driver_name")
    private String driverName;
    @Column(name = "phone_number", length = 10)
    private String phoneNumber;
    @Column(name = "driver_status")
    private boolean status;
    @Column(length = 5)
    private int average_rating;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "driver")
    private Set<Car> cars;
    @OneToOne
    @JoinColumn(name = "password_id", nullable = false)
    private SecurePassword password;
    @OneToOne
    @JoinColumn(name = "card_id", nullable = false)
    private CreditCard card;
    @ManyToMany(mappedBy = "drivers")
    private Set<OrderDetail> orderDetails = new HashSet<>();

    public Driver() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(int average_rating) {
        this.average_rating = average_rating;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

    public SecurePassword getPassword() {
        return password;
    }

    public void setPassword(SecurePassword password) {
        this.password = password;
    }

    public CreditCard getCard() {
        return card;
    }

    public void setCard(CreditCard card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id=" + id +
                ", driverName='" + driverName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", status=" + status +
                ", average_rating=" + average_rating +
                ", cars=" + cars +
                ", password=" + password +
                ", card=" + card +
                '}';
    }
}

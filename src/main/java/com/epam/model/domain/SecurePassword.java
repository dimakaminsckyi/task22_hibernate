package com.epam.model.domain;

import javax.persistence.*;

@Entity
@Table(name = "secure_password")
public class SecurePassword {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "password_str", length = 16)
    private String password;
    @OneToOne(mappedBy = "password")
    private BusinessCustomer businessCustomer;
    @OneToOne(mappedBy = "password")
    private Driver driver;

    public SecurePassword() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BusinessCustomer getBusinessCustomer() {
        return businessCustomer;
    }

    public void setBusinessCustomer(BusinessCustomer businessCustomer) {
        this.businessCustomer = businessCustomer;
    }

    @Override
    public String toString() {
        return "SecurePassword{" +
                "id=" + id +
                ", password='" + password + '\'' +
                '}';
    }
}

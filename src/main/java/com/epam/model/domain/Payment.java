package com.epam.model.domain;

public enum Payment {

    CARD,
    CASH
}

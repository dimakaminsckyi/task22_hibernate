package com.epam.model.domain;

public enum CarClass {

    COMFORT,
    BUSINESS
}

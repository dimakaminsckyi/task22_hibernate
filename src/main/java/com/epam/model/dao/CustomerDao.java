package com.epam.model.dao;

import com.epam.model.domain.Customer;

public interface CustomerDao extends GeneralDao<Customer, Integer> {
}

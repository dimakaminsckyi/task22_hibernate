package com.epam.model.dao.impl;

import com.epam.model.dao.CustomerDao;
import com.epam.model.domain.Customer;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;

public class CustomerDaoImpl implements CustomerDao {

    private Session session;

    public CustomerDaoImpl(Session session) {
        this.session = session;
    }

    @Override
    public List<Customer> findAll() throws SQLException {
        return (List<Customer>) session.createQuery("FROM Customer").list();
    }

    @Override
    public Customer findById(Integer integer) throws SQLException {
       return session.load(Customer.class, integer);
//        return session.get(Customer.class, integer);
    }

    @Override
    public void create(Customer entity) throws SQLException {
        session.getTransaction().begin();
        session.save(entity);
        session.getTransaction().commit();
    }

    @Override
    public void update(Customer entity) throws SQLException {
        session.getTransaction().begin();
        session.update(entity);
        session.getTransaction().commit();
    }

    @Override
    public void delete(Customer entity) throws SQLException {
        session.getTransaction().begin();
        session.remove(entity);
        session.getTransaction().commit();
    }
}

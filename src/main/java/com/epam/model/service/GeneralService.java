package com.epam.model.service;

import java.sql.SQLException;

public interface GeneralService {

    void showAll() throws SQLException;

    void showById() throws SQLException;

    void delete() throws SQLException;

    void create() throws SQLException;

    void update() throws SQLException;
}

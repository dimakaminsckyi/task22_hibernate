package com.epam.model.service.impl;

import com.epam.model.dao.CustomerDao;
import com.epam.model.dao.impl.CustomerDaoImpl;
import com.epam.model.domain.Customer;
import com.epam.model.domain.SecurePassword;
import com.epam.model.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.Scanner;

public class CustomerServiceImpl implements CustomerService {

    private Logger log = LogManager.getLogger(CustomerServiceImpl.class);
    private CustomerDao dao;
    private Scanner scanner;

    public CustomerServiceImpl(Session session) {
        dao = new CustomerDaoImpl(session);
        scanner = new Scanner(System.in);
    }

    @Override
    public void showAll() throws SQLException {
        dao.findAll().forEach(log::info);
    }

    @Override
    public void showById() throws SQLException {
        log.info("Enter id : ");
        log.info(dao.findById(scanner.nextInt()));
    }

    @Override
    public void delete() throws SQLException {
        log.info("Enter id : ");
        Customer customer = dao.findById(scanner.nextInt());
        dao.delete(customer);
    }

    @Override
    public void create() throws SQLException {
        dao.create(generateCustomer());
    }

    @Override
    public void update() throws SQLException {
        log.info("Enter id customer :");
        Customer customer = generateCustomer();
        customer.setId(scanner.nextInt());
        dao.update(customer);
    }


    private Customer generateCustomer(){
        log.info("Enter customer new name : ");
        String name = scanner.next();
        log.info("Enter phone number : ");
        String phoneNumber = scanner.next();
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        customer.setCustomerName(name);
        customer.setPassword(generatePassword());
        return customer;
    }

    private SecurePassword generatePassword(){
        log.info("Enter password :");
        SecurePassword securePassword = new SecurePassword();
        securePassword.setPassword(scanner.next());
        return securePassword;
    }
}

package com.epam.view;

import com.epam.controller.Controller;
import com.epam.util.SessionUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;
    private Scanner scanner ;
    private Session session;

    public MainView() {
        initMenu();
        session = SessionUtil.getSession();
        controller = new Controller(session);
        scanner = new Scanner(System.in);
        methodRun = new LinkedHashMap<>();
        methodRun.put("1", () -> controller.chooseOperation());
        methodRun.put("exit", () -> log.info("Bye"));
    }

    public void show() {
        String flag = "";
        do {
            menu.values().forEach(s -> log.info(s));
            try {
                flag = scanner.next();
                methodRun.get(flag).run();
            } catch (NullPointerException e) {
                log.warn("Input correct option");
            }
        }while (!flag.equals("exit"));
        scanner.close();
    }

    private void initMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Customer CRUD");
        menu.put("exit", "exit - Exit");
    }
}


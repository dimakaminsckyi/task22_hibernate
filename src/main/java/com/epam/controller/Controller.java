package com.epam.controller;

import com.epam.model.service.CustomerService;
import com.epam.model.service.impl.CustomerServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.Scanner;

public class Controller {

    private Logger log = LogManager.getLogger(Controller.class);
    private CustomerService service;

    public Controller(Session session) {
        service = new CustomerServiceImpl(session);
    }

    public void chooseOperation(){
        Scanner scanner = new Scanner(System.in);
        log.info("1 - Create");
        log.info("2 - Update");
        log.info("3 - Show all");
        log.info("4 - find by id");
        log.info("5 - delete by id");
        try {
            switch (scanner.nextInt()){
                case 1 :
                    service.create();
                    break;
                case 2:
                    service.update();
                    break;
                case 3:
                    service.showAll();
                    break;
                case 4:
                    service.showById();
                    break;
                case 5:
                    service.delete();
                    break;
                default:
                    log.error("Enter correct option");
            }
        } catch (SQLException e) {
            log.error("Wrong input");
        }
    }
}

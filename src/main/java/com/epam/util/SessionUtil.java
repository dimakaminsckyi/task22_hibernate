package com.epam.util;

import com.epam.model.domain.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Properties;

public class SessionUtil {

    private static final SessionFactory concreteSessionFactory;

    public SessionUtil() {
    }

    static {
        try {
            Properties prop= new Properties();
            prop.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
            prop.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
            prop.setProperty("hibernate.connection.url",
                            "jdbc:mysql://localhost:3306/mydb" +
                                    "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
            prop.setProperty("hibernate.connection.username", "root");
            prop.setProperty("hibernate.connection.password", "123123qwe");
            prop.setProperty("hibernate.hbm2ddl.auto","create-drop");
            prop.setProperty("hibernate.connection.autocommit", "true");
            Configuration config = new Configuration();
            config.setProperties(prop);
            config.addAnnotatedClass(BusinessCustomer.class);
            config.addAnnotatedClass(Car.class);
            config.addAnnotatedClass(SecurePassword.class);
            config.addAnnotatedClass(CreditCard.class);;
            config.addAnnotatedClass(OrderDetail.class);
            config.addAnnotatedClass(Customer.class);
            config.addAnnotatedClass(Driver.class);

            concreteSessionFactory = config.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return concreteSessionFactory.openSession();
    }
}
